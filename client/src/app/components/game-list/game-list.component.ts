import { Component, OnInit } from '@angular/core';
import { GamesService } from '../../services/games.service'
import { Game } from '../../models/Game'
import { Router } from '@angular/router';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  private games: Game;

  public Setgames(gamees) {
    console.log(gamees.length)
    if(gamees.length !== 0){
      this.games = gamees;
    }else{
    this.games = null;
    }
  }

  constructor(private gamesService: GamesService, private router: Router) { }

  ngOnInit() {
    this.getGames()
  }

  getGames(){
    this.gamesService.getGames().subscribe(
      res =>{ this.Setgames(res), console.log(res)},
      err => console.log(err)
    )
  }


  deleteGame(id: string){


    Swal.fire({
      title: 'Estas Seguro?',
      text: "Seguro que quieres Eliminar el juego?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, estoy seguro!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Eliminado!',
          'Se ha eliminado el juego ',
          'success'
        )

      this.gamesService.removeGame(id)
      .subscribe(
        res =>{
          console.log(res);
          this.getGames();
        },
        err => console.log(err)
      )
      }
    })

  }









   
}
