import { Component, OnInit } from '@angular/core';
import { Game } from '../../models/Game'

import { GamesService } from '../../services/games.service'
import { ActivatedRoute, Router } from '@angular/router'

import Swal from 'sweetalert2'

@Component({
  selector: 'app-game-form',
  templateUrl: './game-form.component.html',
  styleUrls: ['./game-form.component.css']
})
export class GameFormComponent implements OnInit {



  game: Game = {
    id:0,
    title:'',
    description:'',
    image:'',
    created_at: new Date()
  }
  edit:boolean=false;


  constructor(private gamesService: GamesService, 
              private router: Router,
              private activeroute: ActivatedRoute
              ) { }

  ngOnInit() {
    const params = this.activeroute.snapshot.params
    if(params.id){
      this.gamesService.getGame(params.id)
        .subscribe(
          res=>{
            this.game = res;
            this.edit=true
          },
          err => console.log(err)
          
          )
    }
  }


  saveGame(){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
    
    delete this.game.created_at;
    delete this.game.id;
    this.gamesService.saveGame(this.game)
      .subscribe(
        res => {
          console.log(res);
          Toast.fire({
          type: 'success',
          title: 'El juego se guardo correctamente'
    })
          this.router.navigate(['/games']);
        },
        err => {console.log(err);
        Toast.fire({
            type: 'warning',
            title:"Error al guardar",
            text:'Por algna razon no se pude guarda el juego'
          })
        }
      )
  }

  updateGame(){

    Swal.fire({
      title: 'Estas Seguro?',
      text: "Seguro que quieres guardar los cambios?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, estoy seguro!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Guardado!',
          'Se guardaron los cambios ',
          'success'
        )

      delete this.game.created_at;
        this.gamesService.updateGame(this.game.id,this.game)
      .subscribe(
        res=>{
          this.router.navigate(['/games']);
        },
        err => console.log(err)
      )
      }
    })

  }

}
