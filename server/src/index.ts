/*  npm in @type/express, morgan y cors */
import express, {Application} from 'express';
import morgan from 'morgan';
import cors from 'cors'
import index from './routes/indexRoutes';
import games from './routes/gamesRoutes';

class Server {
    
    public app: Application;

    constructor(){
        this.app = express(); // crea servidor
        this.config(); // inicia configuracion
        this.routes(); // inicia rutas
    }

    config(): void { 
        this.app.set('port', process.env.PORT || 3000); // seteo el puerto si no hay uno definido
        this.app.use(morgan('dev')); // ver las peticiones en consola
        this.app.use(cors()); // permite comunicar 2 servidor
        this.app.use(express.json()); // acepta formato json en el servidor (el bodyparse)
        this.app.use(express.urlencoded({extended:false})); // enviar desde formulario html
    }
    
    routes(): void {
        this.app.use('/',index);
        this.app.use('/api/games',games)
    }

    start(): void{
        // inicia el servidor y escucha el puerto
        this.app.listen(this.app.get('port'),()=>{
        console.log(`Server on port`,this.app.get('port'));
        })
    }
}

const server  = new Server();
server.start()