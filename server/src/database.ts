import mysql from 'promise-mysql';
import keys from './keys'

//crear conexion
const pool = mysql.createPool(keys.database);

// obtener conexion con promise
pool.getConnection()
    .then(connection =>{
        pool.releaseConnection(connection);
        console.log('DATABASE conectada')
    })
    .catch(error =>{ console.log('NO se pudo conectar la base de datos',error)});

    // exportamos la conexion
    export default pool;