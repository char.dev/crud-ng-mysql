import { Request, Response } from 'express'
import db from '../database'

class GamesController{
    
    /*public index (req : Request ,res: Response) {
        db.query('DESCRIBE games')
        res.json('Bienvenido API Games')
    }*/
    
    public async list (req : Request ,res: Response){
        const games = await db.query('SELECT * FROM games');
        res.json(games)
    }

    public async getOne(req : Request ,res: Response): Promise<any>{
        const { id } = req.params
        const games = await db.query('SELECT * FROM games WHERE id=?', [id]);
        if(games.length > 0){
             res.json(games[0]);
        }
        res.status(404).json("ERROR: Juego no encontrado")
       
    }

    public async create (req : Request ,res: Response): Promise<void>{
        console.log(req.body)
        await db.query('INSERT INTO games set ?', [req.body])
        res.json('creando un juego')
    }

    public async delete (req : Request ,res: Response): Promise<void>{
        const { id } = req.params;
        await db.query('DELETE FROM games WHERE id = ?', [id]);
        res.json('Juego Eliminado')
    }

    public async update (req : Request ,res: Response): Promise<void>{
        const { id } = req.params;
        await db.query('UPDATE games set ? WHERE id = ?', [req.body, id]);
        res.json('Juego Actualizado')
    }
}

export const gamesController = new GamesController();