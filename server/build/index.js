"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*  npm in @type/express, morgan y cors */
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const indexRoutes_1 = __importDefault(require("./routes/indexRoutes"));
const gamesRoutes_1 = __importDefault(require("./routes/gamesRoutes"));
class Server {
    constructor() {
        this.app = express_1.default(); // crea servidor
        this.config(); // inicia configuracion
        this.routes(); // inicia rutas
    }
    config() {
        this.app.set('port', process.env.PORT || 3000); // seteo el puerto si no hay uno definido
        this.app.use(morgan_1.default('dev')); // ver las peticiones en consola
        this.app.use(cors_1.default()); // permite comunicar 2 servidor
        this.app.use(express_1.default.json()); // acepta formato json en el servidor (el bodyparse)
        this.app.use(express_1.default.urlencoded({ extended: false })); // enviar desde formulario html
    }
    routes() {
        this.app.use('/', indexRoutes_1.default);
        this.app.use('/api/games', gamesRoutes_1.default);
    }
    start() {
        // inicia el servidor y escucha el puerto
        this.app.listen(this.app.get('port'), () => {
            console.log(`Server on port`, this.app.get('port'));
        });
    }
}
const server = new Server();
server.start();
