"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const promise_mysql_1 = __importDefault(require("promise-mysql"));
const keys_1 = __importDefault(require("./keys"));
//crear conexion
const pool = promise_mysql_1.default.createPool(keys_1.default.database);
// obtener conexion con promise
pool.getConnection()
    .then(connection => {
    pool.releaseConnection(connection);
    console.log('DATABASE conectada');
})
    .catch(error => { console.log('NO se pudo conectar la base de datos', error); });
// exportamos la conexion
exports.default = pool;
